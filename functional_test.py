import time
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By

browser = webdriver.Chrome()
browser.get('http://coverage100.herokuapp.com/YourDay/')

assert 'How was your day?' in browser.title
assert 'Coba-coba' in browser.page_source
title = browser.find_element_by_tag_name("h1").get_property("style")

status = browser.find_element_by_name("status")
status.send_keys("Coba-coba")
status.submit()

assert 'Coba-coba' in browser.page_source

time.sleep(5)
browser.quit()
