from django.urls import path
from .views import *
from django.contrib.staticfiles.urls import staticfiles_urlpatterns

#url for app

urlpatterns = [
    path('MyProfile/', show_profile, name = 'show_profile'),
    path('YourDay/', index, name='index'),
    path('ChooseMyDay/', choosen_choice, name='choosen_choice'),
    path('', show_profile, name = "landing"),
]