from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import *
from .models import AboutMyDay
from django.http import HttpRequest
import time
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options


# Create your tests here.

class MyStoryUnitTest(TestCase):
    def test_my_story_url_is_exist(self):
        response = Client().get('/YourDay/')
        self.assertEqual(response.status_code, 200)

    def test_my_story_using_index_function(self):
        found = resolve('/YourDay/')
        self.assertEqual(found.func, index)

    def test_using_index_template(self):
        response = Client().get('/YourDay/')
        self.assertTemplateUsed(response, 'index.html')

    def test_my_profile_url_is_exist(self):
        response = Client().get('/MyProfile/')
        self.assertEqual(response.status_code, 200)

    def test_my_story_using_show_profile_function(self):
        found = resolve('/MyProfile/')
        self.assertEqual(found.func, show_profile)

    def test_using_profile_template(self):
        response = Client().get('/MyProfile/')
        self.assertTemplateUsed(response, 'profile.html')

    def test_choose_my_day_url_is_exist(self):
        response = Client().get('/ChooseMyDay/')
        self.assertEqual(response.status_code, 302)

    def test_my_story_using_choose_my_day_function(self):
        found = resolve('/ChooseMyDay/')
        self.assertEqual(found.func, choosen_choice)

    def test_for_landing_page_content(self):
        self.assertIsNotNone(landing_page_content)
        self.assertTrue(len(landing_page_content) >= 20)

    def test_landing_page_is_completed(self):
        request = HttpRequest()
        response = index(request)
        html_response = response.content.decode('utf8')
        self.assertIn(landing_page_content, html_response)

    def test_my_story_model(self):
        AboutMyDay.objects.create(status="Aku baik")
        counter = AboutMyDay.objects.all().count()
        self.assertEqual(counter,1)

    def test_can_save_a_POST_request(self):

        response = self.client.post('/ChooseMyDay/', data={'status': 'Aku baik-baik saja'})
        counting_all_available_activity = AboutMyDay.objects.all().count()
        self.assertEqual(counting_all_available_activity, 1)

        self.assertEqual(response.status_code, 200)

        new_response = self.client.get('/ChooseMyDay/')
        html_response = new_response.content.decode('utf8')
        self.assertIn('', html_response)

"""class FunctionalTestStory(TestCase):
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        service_log_path = "./chromedriver.log"
        service_args = ['--verbose']
        self.browser = webdriver.Chrome()
    def tearDown(self):
        self.browser.quit()
    def test_can_save_a_form(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        service_log_path = "./chromedriver.log"
        service_args = ['--verbose']
        self.browser = webdriver.Chrome()
        browser = webdriver.Chrome()
        browser.get('http://coverage100.herokuapp.com/YourDay/')

        self.assertIn('How was your day?', browser.title)
        self.assertIn('Coba-coba', browser.page_source)

        title = browser.find_element_by_tag_name("h1").value_of_css_property('text-align')
        content = browser.find_element_by_class_name("mx-auto").value_of_css_property('width')
        self.assertEqual('center', title)
        self.assertEqual('500px', content)

        backgr = browser.find_element_by_tag_name("body").value_of_css_property('background-image')
        color_1 =  browser.find_element_by_tag_name("h1").value_of_css_property('color')
        self.assertEqual('url("http://coverage100.herokuapp.com/static/geo.jpg")', backgr)
        self.assertEqual('rgba(33, 37, 41, 1)', color_1)

        status = browser.find_element_by_name("status")
        status.send_keys("Coba-coba")
        status.submit()

        self.assertIn('Coba-coba', browser.page_source)

        time.sleep(5)
        browser.quit()

"""
