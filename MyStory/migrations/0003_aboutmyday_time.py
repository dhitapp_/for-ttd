# Generated by Django 2.1.1 on 2018-10-11 06:37

from django.db import migrations, models
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('MyStory', '0002_auto_20181011_0901'),
    ]

    operations = [
        migrations.AddField(
            model_name='aboutmyday',
            name='time',
            field=models.DateTimeField(auto_now_add=True, default=django.utils.timezone.now),
            preserve_default=False,
        ),
    ]
