from django import forms

class MyDayToday(forms.Form):

    status = forms.CharField(label='Status', max_length=399, widget=forms.Textarea(attrs = {'class': 'form-control'}), required = True)