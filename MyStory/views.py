from django.shortcuts import render
from datetime import datetime
from django.http import HttpResponseRedirect
from .forms import MyDayToday
from .models import AboutMyDay

# Create your views here.

response = {}
landing_page_content = "Hello! How was your day?"

def index(request):
    response = {'MyDayToday' : MyDayToday, 'data' : AboutMyDay.objects.all().values()}
    return render(request, 'index.html', response)

def show_profile(request):
    return render(request, 'profile.html', response)

def choosen_choice(request):
    form = MyDayToday(request.POST or None)
    response = {}
    
    if (request.method == 'POST' and form.is_valid):
        response['status'] = request.POST['status']
        print(request.POST['status'])
        my_day = AboutMyDay(status=response['status'])
        my_day.save()
        response = {'MyDayToday' : MyDayToday, 'data' : AboutMyDay.objects.all().values()}
        return render(request, 'index.html', response)
    else:
        response['data'] = AboutMyDay.objects.all().values()        
        return HttpResponseRedirect('')

